from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
import calendar
from calendar import HTMLCalendar
from datetime import datetime

# Create your views here.


@login_required
def list_projects(request):
    list_project = Project.objects.filter(owner=request.user)
    context = {"list_project": list_project}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "this_project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def calendar_view(request, year=datetime.now().year, month=datetime.now().strftime('%B')):
    month = month.title()
    month_number = list(calendar.month_name).index(month)
    month_number = int(month_number)
    cal = HTMLCalendar().formatmonth(
        year,
        month_number)
    now = datetime.now()
    current_year = now.year
    current_time = now.strftime('%I:%M %p')
    context = {
        "year": year,
        "month": month,
        "month_number": month_number,
        "cal": cal,
        "current_year": current_year,
        "current_time": current_time,
    }
    return render(request, 'projects/calendar.html', context)


@login_required
def calendar_any(request, year, month):
    month = month.title()
    month_number = list(calendar.month_name).index(month)
    month_number = int(month_number)
    cal = HTMLCalendar().formatmonth(
        year,
        month_number)
    now = datetime.now()
    current_year = now.year
    current_time = now.strftime('%I:%M %p')
    context = {
        "year": year,
        "month": month,
        "month_number": month_number,
        "cal": cal,
        "current_year": current_year,
        "current_time": current_time,
    }
    return render(request, 'projects/calendar.html', context)
