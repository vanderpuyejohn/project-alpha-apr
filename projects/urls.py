from django.urls import path
from projects.views import list_projects, show_project, create_project
from projects.views import calendar_view, calendar_any

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path('<int:year>/<str:month>/', calendar_any, name="year_calendar"),
    path('calendar/', calendar_view, name="event_calendar"),

]
